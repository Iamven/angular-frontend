import {Component} from '@angular/core';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html'
})
export class TemplateComponent {
  sidebarActive: boolean;
  onMenuButtonClick(event: Event) {
      this.sidebarActive = !this.sidebarActive;

      event.preventDefault();
  }
}
